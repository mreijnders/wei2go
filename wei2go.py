##
## Wei2GO: weighted sequence similarity-based protein function prediction
## Author: MJMF Reijnders
##

import sys
from collections import defaultdict
from math import log
from math import log2
from math import log10
import numpy
import os

## Input: Wei2GO directory
## Output: a dictionary containing GO parent relations (parents_dict), and a dictionary containing GO parents relations which will later be filled with additional child relationships
##
## Creates a dictionary containing all child-parent relationships for GO terms
def get_go_slim_parents(wei2go_path):
	parents_dict = defaultdict(list)
	go_slim_dict = defaultdict(list)
	for line in open(wei2go_path+'/data/goParents.tab'):
		ssline = line.strip().split('\t')
		child = ssline[0]
		parent = ssline[1]
		if not child == parent:
			parents_dict[child].append(parent)
			go_slim_dict[child].append(parent)
	return(parents_dict,go_slim_dict)

## Input: Wei2GO directory, go_slim_dict which contains GO relations
## Output: a dictionary containing GO child relations ('children_dict'), and a dictionary containing both GO parent and child relations ('go_slim_dict')
##
## Create a dictionary containing all children relationships of GO terms
def get_go_slim_children(wei2go_path,go_slim_dict):
	children_dict = defaultdict(list)
	for line in open(wei2go_path+'/data/goChildren.tab'):
		ssline = line.strip().split('\t')
		child = ssline[1]
		parent = ssline[0]
		if not child == parent:
			children_dict[parent].append(child)
			go_slim_dict[parent].append(child)
	return(children_dict,go_slim_dict)

## Input: Wei2GO directory
## Output: 'root_node_dict' which contains the root node of a GO term (biological process, molecular function, or cellular component)
##
## Get a root node associated to each GO term
def get_root_nodes(wei2go_path):
	root_node_dict = {}
	for line in open(wei2go_path+'/data/nameSpaces.tab'):
		ssline = line.strip().split('\t')
		go_term = ssline[0]
		root_node = ssline[1]
		if root_node == 'biological_process':
			root_node_dict[go_term] = 'GO:0008150'
		elif root_node == 'molecular_function':
			root_node_dict[go_term] = 'GO:0003674'
		elif root_node == 'cellular_component':
			root_node_dict[go_term] = 'GO:0005575'
		else:
			root_node_dict[go_term] = 'GO:0008150'
	root_node_dict['GO:0008150'] = 'GO:0008150'
	root_node_dict['GO:0003674'] = 'GO:0003674'
	root_node_dict['GO:0005575'] = 'GO:0005575'
	return(root_node_dict)

## Input: Wei2GO directory, dictionary containing the root nodes of each GO term, dictionary containing the child relations for each GO term
## Output: dictionary containing the number of times a GO term occurs in the database ('go_counts_dict'), and a list which will be used to check if a GO term is eligible for being predicted ('go_eligibility_list')
##
## Create a dictionary containing the number of occurrences of each GO term in the uniprot database
def get_go_counts(wei2go_path,root_node_dict,children_dict):
	go_counts_dict = defaultdict(int)
	for line in open(wei2go_path+'/data/goCounts.tab'):
		go_term,counts = line.strip().split('\t')
		counts = int(counts)
		go_counts_dict[go_term] = counts
	root_list = ['GO:0008150','GO:0003674','GO:0005575']
	for root_node in root_list:
		root_children = children_dict[root_node]
		for root_child in root_children:
			go_counts_dict[root_node] += float(go_counts_dict[root_child])
	go_eligibility_list = go_counts_dict.keys()
	return(go_counts_dict,go_eligibility_list)

## Input: Wei2GO directory, blast input file path, list of GO terms eligible for being predicted
## Output: dictionary containing all protein-GO term relations derived from BLAST hits, and their associated weights, BLAST hits, and evalues ('prot_to_go_dict_blast'), and a counter ('count') which tracks the number of GO terms in the database annotated using the 'IEA' evidence code
##
## Read the DIAMOND input file and create a dictionary which associates all GO IEA annotations to proteins
def readBlast_IEA(wei2go_path,blast_file_path,go_eligibility_list):
	prot_to_go_dict_blast = defaultdict(dict)
	prot_to_go_dict_goa_pfam = defaultdict(set)
	sseq_set = set([])
	for line in open(blast_file_path):
		ssline = line.strip().split('\t')
		sseq_id = ssline[1]
		sseq_id_short = sseq_id.split('|')[1]
		sseq_set.add(sseq_id_short)
	
	count = 0
	for line in open(wei2go_path+'/data/goaUniprot_IEA.tab'):
		ssline = line.strip().split('\t')
		prot = ssline[0]
		count += 1
		if prot in sseq_set:
			go_term = ssline[1]
			prot_to_go_dict_goa_pfam[prot].add(go_term)
	
	for line in open(blast_file_path):
		ssline = line.strip().split('\t')
		qseq_id = ssline[0]
		sseq_id = ssline[1]
		sseq_id_short = sseq_id.split('|')[1]
		evalue = float(ssline[-2])
		go_list = prot_to_go_dict_goa_pfam[sseq_id_short]
		for go_term in go_list:
			if go_term in go_eligibility_list:
				if evalue <= 1:
					if evalue == 0.0:
						evalue = 2.225074e-308
					weight = abs(-log10(evalue))
					prot_to_go_dict_blast[qseq_id].setdefault(go_term,[]).append([sseq_id,evalue,weight])
	return(prot_to_go_dict_blast,count)

## Input: Wei2GO directory, input BLAST file path, list of GO terms eligible for being predicted
## Output: dictionary containing all protein-GO term relations derived from BLAST hits, and their associated weights, BLAST hits, and evalues ('prot_to_go_dict_blast'), and a counter ('count') which tracks the number of GO terms in the database annotated not using the 'IEA' evidence code
##
## Read the DIAMOND input file and create a dictionary which associates all GO annotations that are not IEA to proteins
def readBlast_nonIEA(wei2go_path,blast_file_path,go_eligibility_list):
        prot_to_go_dict_blast = defaultdict(dict)
        prot_to_go_dict_goa_pfam = defaultdict(set)
        sseq_set = set([])
        for line in open(blast_file_path):
                ssline = line.strip().split('\t')
                sseq_id = ssline[1]
                sseq_id_short = sseq_id.split('|')[1]
                sseq_set.add(sseq_id_short)

        print('Matching BLAST to GOA')
        count = 0
        for line in open(wei2go_path+'/data/goaUniprot_noIEA.tab'):
                ssline = line.strip().split('\t')
                prot = ssline[0]
                count += 1
                if prot in sseq_set:
                        go_term = ssline[1]
                        prot_to_go_dict_goa_pfam[prot].add(go_term)

        for line in open(blast_file_path):
                ssline = line.strip().split('\t')
                qseq_id = ssline[0]
                sseq_id = ssline[1]
                sseq_id_short = sseq_id.split('|')[1]
                evalue = float(ssline[-2])
                go_list = prot_to_go_dict_goa_pfam[sseq_id_short]
                for go_term in go_list:
                        if go_term in go_eligibility_list:
                                if evalue <= 1:
                                    if evalue == 0.0:
                                            evalue = 2.225074e-308
                                    weight = abs(log10(evalue))
                                    prot_to_go_dict_blast[qseq_id].setdefault(go_term,[]).append([sseq_id,evalue,weight])
        return(prot_to_go_dict_blast,count)

## Input: Wei2GO directory
## Output: dictionary containing all Pfam-GO relations derived from the pfam2go file ('pfam2go_dict')
##
## Read the pfam2go file into a dictionary
def read_pfam2go(wei2go_path):
	pfam2go_dict = defaultdict(list)
	for line in open(wei2go_path+'/data/pfam2go'):
		if not line.startswith('!'):
			ssline = line.strip().split(' ')
			pfam_id = ssline[1]
			go = ssline[-1]
			pfam2go_dict[pfam_id].append(go)
	return(pfam2go_dict)

## Input: Wei2GO directory, input HMMScan file path, list of eligible GO terms to predict
## Output: dictionary containig all HMMScan derived GO terms with all their weights, evalues, and Pfam ID's of origin ('prot_to_go_dict_hmmscan')
##
## Read the HMMER input file into a dictionary associating proteins to GO terms taken from pfam2go
def read_hmmscan(wei2go_path,hmmscan_file_path,go_eligibility_list):
	print('Matching HMMScan to GOA')
	pfam2go_dict = read_pfam2go(wei2go_path)
	prot_to_go_dict_hmmscan = defaultdict(dict)
	pfam2go_dict = read_pfam2go(wei2go_path)
	for line in open(hmmscan_file_path):
		if not line.startswith('#'):
			ssline = ' '.join(line.split()).split(' ')
			#ssline = line.strip().split('\t')
			qseq_id = ssline[2]
			pfam_id = ssline[0]
			evalue = float(ssline[4])
			if evalue <= 1:
				if evalue < 1e250:
					evalue = 2.225074e-308
				go_list = pfam2go_dict[pfam_id]
				if not go_list == []:
					weight = abs(-log10(evalue))
					for go_term in go_list:
						if go_term in go_eligibility_list:
							prot_to_go_dict_hmmscan[qseq_id].setdefault(go_term,[]).append([pfam_id,evalue,weight])
	return(prot_to_go_dict_hmmscan)

## Input: protein annotation dict derived from BLAST hits based on the IEA evidence code ('prot_to_go_dict_blast_IEA'), protein annotation dict derived from BLAST hits based on non-IEA evidence codes ('prot_to_go_dict_blast_nonIEA'), protein annotantio dict derived from HMMScan hits ('prot_to_go_dict_hmmscan'), number of GO terms in the database with an IEA evidence code ('iea_count'), and number of GO terms in the database with a non-IEA evidence code ('nonIEA_count')
## Output: a protein-GO term dictionary including each BLAST or HMMScan hit for each GO term ('prot_to_go_dict')
##
## Merge the previously created dictionary structures for BLAST with IEA terms as well as total weighted score, BLAST without IEA terms, and HMMSCAN with pfam terms into one merged dictionary associating proteins to GO terms
def merge_annotations(prot_to_go_dict_blast_IEA,prot_to_go_dict_blast_nonIEA,prot_to_go_dict_hmmscan,iea_count,nonIEA_count):
	print('Merging BLAST and HMMScan annotations')
	prot_to_go_dict = defaultdict(dict)
	modifier = log2(iea_count/nonIEA_count)
	for prot,go_terms in prot_to_go_dict_blast_IEA.items(): ## Iterate over the BLAST with IEA terms dictionary, and merge with BLAST non-IEA GO terms and HMMER GO terms
		for go_term in go_terms:
			total_weight = 0
			hmmscan_hits = []
			nonIEA_hits = []
			blast_hits = []
			blast_hits = prot_to_go_dict_blast_IEA[prot][go_term]
			for sseq_id in prot_to_go_dict_blast_IEA[prot][go_term]:
				weight = sseq_id[-1]
				total_weight += weight
			if prot in prot_to_go_dict_blast_nonIEA:
				if go_term in prot_to_go_dict_blast_nonIEA[prot]:
					for sseq_id in prot_to_go_dict_blast_nonIEA[prot][go_term]:
						weight = sseq_id[-1]
						total_weight += weight*modifier
					nonIEA_hits = prot_to_go_dict_blast_nonIEA[prot][go_term]
			if prot in prot_to_go_dict_hmmscan:
				if go_term in prot_to_go_dict_hmmscan[prot]:
					for sseq_id in prot_to_go_dict_hmmscan[prot][go_term]:
						weight = sseq_id[-1]
						total_weight += weight*modifier
					hmmscan_hits = prot_to_go_dict_hmmscan[prot][go_term]
			try:
				prot_to_go_dict[prot].setdefault(go_term,[]).append(total_weight)
				prot_to_go_dict[prot][go_term].append(blast_hits)
				prot_to_go_dict[prot][go_term].append(nonIEA_hits)
				prot_to_go_dict[prot][go_term].append(hmmscan_hits)
			except:
				continue

	for prot,go_terms in prot_to_go_dict_blast_nonIEA.items(): ## Iterate over the remaining proteins in the BLAST non-IEA dictionary and merge with HMMER GO terms
		for go_term in go_terms:
			if not go_term in prot_to_go_dict[prot]:
				total_weight = 0
				nonIEA_hits = prot_to_go_dict_blast_nonIEA[prot][go_term]
				hmmscan_hits = []
				for sseq_id in nonIEA_hits:
					weight = sseq_id[-1]
					total_weight += weight*modifier
				if prot in prot_to_go_dict_hmmscan:
					if go_term in prot_to_go_dict_hmmscan[prot]:
						for sseq_id in prot_to_go_dict_hmmscan[prot][go_term]:
							weight = sseq_id[-1]
							total_weight += weight*modifier
						hmmscan_hits = prot_to_go_dict_hmmscan[prot][go_term]
				try:
					prot_to_go_dict[prot].setdefault(go_term,[]).append(total_weight)
					prot_to_go_dict[prot][go_term].append([])
					prot_to_go_dict[prot][go_term].append(nonIEA_hits)
					prot_to_go_dict[prot][go_term].append(hmmscan_hits)
				except:
					continue

	for prot,go_terms in prot_to_go_dict_hmmscan.items(): ## Iterate over the remaining HMMER proteins and add to dictionary as solo entries, since the other dictionaries are now empty
		for go_term in go_terms:
			if not go_term in prot_to_go_dict[prot]:
				total_weight = 0
				hmmscan_hits = prot_to_go_dict_hmmscan[prot][go_term]
				for sseq_id in hmmscan_hits:
					weight = float(sseq_id[-1])
					total_weight += weight*modifier
				try:
					prot_to_go_dict[prot].setdefault(go_term,[]).append(total_weight)
					prot_to_go_dict[prot][go_term].append([])
					prot_to_go_dict[prot][go_term].append([])
					prot_to_go_dict[prot][go_term].append(hmmscan_hits)
				except:
					continue
	return(prot_to_go_dict)

## Input: protein annotation dict ('prot_to_go_dict'), GO-child relations dictionary ('children_dict'), dictionary containig the root node of every GO term ('root_node_dict')
## Output: dictionary containig the weights for each protein-GO term annotation ('weight_dict')
##
## For each protein-GO association, get the sum of the weights across all hits (IEA, non-IEA, HMMER)
## Each weight_dict entry is a [protein] containing a [go term] containing a weight
## Root nodes are updated by summing the final weight of each GO term
def get_go_weight_dict(prot_to_go_dict, children_dict, root_node_dict):
	weight_dict = defaultdict(dict)
	for prot,go_terms in prot_to_go_dict.items():
		go_term_checklist_dict = defaultdict(list) ## Dictionary to make sure there GO weights arent added twice to the root weight
		for go_term in go_terms:
			if go_term in root_node_dict:
				root_node = root_node_dict[go_term]
			else: ## If for some reason the root node dictionary doesn't contain the GO term, it is added to the BP root node as this is the biggest one
				root_node_dict[go_term] = 'GO:0008150' 
				root_node = 'GO:0008150'
			total_weight = float(prot_to_go_dict[prot][go_term][0]) ## Summed weight of all GO term hits 
			weight_dict[prot].setdefault(go_term,0) ## Create a new dictionary containing the weights
			weight_dict[prot][go_term] += total_weight
			for go_term2 in go_terms: ## Loop that adds the GO weights of all child terms to the weight dict of a GO term
				if go_term2 in children_dict[go_term] and not go_term2 in go_term_checklist_dict[go_term]:
					total_weight2 = float(prot_to_go_dict[prot][go_term][0])
					weight_dict[prot][go_term] += total_weight2
					go_term_checklist_dict[go_term].append(go_term2)
			if not go_term in go_term_checklist_dict[root_node]: ## If the GO's weight (without children added up) hasn't been added to the root weight yet, add it.
				weight_dict[prot].setdefault(root_node,0)
				weight_dict[prot][root_node] += total_weight
				go_term_checklist_dict[root_node].append(go_term)
	return(weight_dict)

## Input: dictionary containig all the weighted scores for each protein-GO term annotation ('weight_dict'), dictionary containing the root node for each GO term ('root_node_dict')
## Output: dictionary containig the InC score for each protein-GO term annotation ('inc_score_dict')
##
## Calculates the internal confidence score and returns a dictionary associating each protein go term pair to a score
def get_inc_scores(weight_dict,root_node_dict):
	print('Calculating Inc scores')
	inc_score_dict = defaultdict(dict)
	for prot,go_terms in weight_dict.items():
		for go_term in go_terms:
			if not go_term in root_node_dict:
				root_node = 'GO:0008150'
			else:
				root_node = root_node_dict[go_term]
			go_weight = weight_dict[prot][go_term]
			root_go_weight = weight_dict[prot][root_node]
			inc_score = go_weight / root_go_weight
			inc_score_dict[prot][go_term] = inc_score
	return(inc_score_dict)

## Input: GO term to calculate the IC for ('go_term'), dictionary containing the counts for each GO term in the database ('counts_dict'), dictionary containig the root node for each GO term ('root_node_dict'), dictionary containig the child terms of each GO term ('children_dict')
## Output: the IC of a GO term ('ic')
##
## Calculates the IC score for a GO term
def get_ic_score(go_term,go_counts_dict,root_node_dict,children_dict):
	if go_term in root_node_dict:
		root_node = root_node_dict[go_term]
	else:
		root_node = 'GO:0008150'
	total_root_proteins = go_counts_dict[root_node]
	ic = 0
	go_counts = float(go_counts_dict[go_term])
	children = children_dict[go_term]
	for child in children:
		go_counts += float(go_counts_dict[child])
	if go_counts == 0:
		go_counts = 1
	ic = -log10(float(go_counts)/float(total_root_proteins))
	return(ic)

## Input: first GO term to use in the calculation of semantic similarity ('go_term_1'), second GO term to use in the calculation of semantic similarity ('go_term_2'), dictionary containing all the parent terms of a GO term ('parents_dict'), dictionary containing the root nodes of a GO term ('root_node_dict'), dictionary containing all child terms of a GO term ('children_dict'), dictionary containig the counts of each GO term in the database ('go_ncouts_dict')
## Output: semantic similarity between two GO terms ('semantic similarity')
##
## Calculate the semantic similarity between two GO terms
def get_semantic_similarity(go_term_1,go_term_2,parents_dict,root_node_dict,children_dict,go_counts_dict):
	go_term_1_ic = get_ic_score(go_term_1,go_counts_dict,root_node_dict,children_dict)
	go_term_2_ic = get_ic_score(go_term_2,go_counts_dict,root_node_dict,children_dict)
	go_term_1_parents = parents_dict[go_term_1]
	go_term_2_parents = parents_dict[go_term_2]
	if go_term_1_ic > go_term_2_ic:
		highest_ic = go_term_1_ic
		highest_ic_term = go_term_1
	else:
		highest_ic = go_term_2_ic
		highest_ic_term = go_term_2
	semantic_similarity = (2*highest_ic)/(go_term_1_ic+go_term_2_ic)
	return(semantic_similarity)

## Input: dictionary containig the InC score of each protein-GO annotation ('inc_dict'), dictionary containing the root node of every GO term ('root_node_dict'), dictionary containig every child term of a GO term ('children_dict'), dictionary containig the amount of counts of each GO term in the database ('go_counts_dict')
## Output: dictionary containig the Group Score of each protein-GO annotation ('group_score_dict'), dictionary containing the non-cumulative Group Score of each protein-GO annotation (group_score_nc_dict) (this output is legacy code)
##
## Calculate the group score for each protein go term pair
def get_group_scores(inc_dict,parents_dict,root_node_dict,children_dict,go_counts_dict):
	print('Calculating group scores')
	group_score_dict = defaultdict(dict)
	group_score_nc_dict = defaultdict(dict)
	for prot,go_terms in inc_dict.items():
		for go_term in go_terms:
			inc_score = float(inc_dict[prot][go_term])
			group_score_dict[prot].setdefault(go_term,0)
			group_score_dict[prot][go_term] += inc_score
			group_score_nc_dict[prot].setdefault(go_term,0)
			group_score_nc_dict[prot][go_term] += inc_score
			go_parents = parents_dict[go_term]
			for go_parent in go_parents:
				semantic_similarity = get_semantic_similarity(go_term,go_parent,parents_dict,root_node_dict,children_dict,go_counts_dict)
				if semantic_similarity >= 0.7: ## Similar proteins for a group currently set at 0.7
					group_score_dict[prot].setdefault(go_parent,0)
					group_score_dict[prot][go_parent] += inc_score
	return(group_score_dict,group_score_nc_dict)

## Input: dictionary containing the weights of all protein-GO annotations ('weight_dict'), dictionary containing all the group scores of a protein-GO annotation ('group_score_dict')
## Output: standard deviation of the group score ('standard deviation')
##
## Standard deviation function for calculating 
##
## This function is legacy code and will be removed in a future version
def get_standard_deviation(weight_dict,group_score_dict):
	list = []
	for prot,go_terms in weight_dict.items():
		for go_term in go_terms:
			list.append(group_score_dict[prot][go_term])
	standard_deviation = numpy.std(list)
	return(standard_deviation)

## Input: dictionary containing protein-GO term annotations ('prot_to_go_dict'), dictionary containig the InC score for a protein-GO annotation ('inc_score_dict'), counts for each GO term in the database ('go_counts _dict'), root node for each GO term ('root_node_dict'), child terms of each GO term ('children_dict'), the GO slim of each GO i.e. all children and parent terms ('go_slim_dict'), group scores of each protein-GO annotation ('group_score_dict')
## Output: function doesn't return output, instead writes predictions and scores directly to a file given by sys.argv[3]
##
## Calculation of the total scores for each protein go term pair, and writes the predictions to the output file
def get_total_scores(prot_to_go_dict,inc_score_dict,go_counts_dict,root_node_dict,children_dict,go_slim_dict,group_score_dict):
	outfile = open(sys.argv[3],'w')
	outfile.write('Protein\tGO term\tScore\n')
	outfile_all = open(sys.argv[3]+'.all_preds','w')
	outfile_all.write('Protein\tGO term\tScore\n')
	for prot,go_terms in prot_to_go_dict.items():
		for go_term in go_terms:
			if go_term in root_node_dict and not go_term == 'GO:0008150' and not go_term == 'GO:0005575' and not go_term == 'GO:0003674':
				go_weight = prot_to_go_dict[prot][go_term][0]
				inc_score = inc_score_dict[prot][go_term]
				group_score = group_score_dict[prot][go_term]
				ic_score = get_ic_score(go_term,go_counts_dict,root_node_dict,children_dict)
				inc_group_score = inc_score/group_score
				total_score = ic_score*inc_group_score*go_weight
				if total_score >= 500:
					outfile.write(prot+'\t'+go_term+'\t'+str(total_score)+'\n')
				outfile_all.write(prot+'\t'+go_term+'\t'+str(total_score)+'\n')
	outfile.close()
	outfile_all.close()
	return

wei2go_path = os.path.dirname(os.path.abspath(__file__))
blast_file_path = sys.argv[1]
hmmscan_file_path = sys.argv[2]
parents_dict,go_slim_dict = get_go_slim_parents(wei2go_path)
children_dict,go_slim_dict = get_go_slim_children(wei2go_path,go_slim_dict)
root_node_dict = get_root_nodes(wei2go_path)
go_counts_dict,go_eligibility_list = get_go_counts(wei2go_path,root_node_dict,children_dict)
prot_to_go_dict_blast_IEA,iea_count = readBlast_IEA(wei2go_path,blast_file_path,go_eligibility_list)
prot_to_go_dict_blast_nonIEA,nonIEA_count = readBlast_nonIEA(wei2go_path,blast_file_path,go_eligibility_list)
prot_to_go_dict_hmmscan = read_hmmscan(wei2go_path,hmmscan_file_path,go_eligibility_list)
prot_to_go_dict = merge_annotations(prot_to_go_dict_blast_IEA,prot_to_go_dict_blast_nonIEA,prot_to_go_dict_hmmscan,iea_count,nonIEA_count)
weight_dict = get_go_weight_dict(prot_to_go_dict,parents_dict,root_node_dict)
inc_score_dict = get_inc_scores(weight_dict,root_node_dict)
group_score_dict,group_score_nc_dict = get_group_scores(inc_score_dict,parents_dict,root_node_dict,children_dict,go_counts_dict)
standard_deviation = get_standard_deviation(weight_dict,group_score_dict)
get_total_scores(prot_to_go_dict,inc_score_dict,go_counts_dict,root_node_dict,children_dict,go_slim_dict,group_score_dict)
