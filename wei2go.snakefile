configfile: 'config.yaml'

rule wei2go:
        input: "infiles/diamond.tab",
        	"infiles/hmmscan.out"
        output: "outfiles/wei2go.tab"
        shell: "python3 wei2go.py {input[0]} {input[1]} {output}"

rule diamond:
	input: config['fasta_file']
	output: "infiles/diamond.tab"
	params: db = config['diamond_uniprotdb']
	shell: "diamond blastp --query {input} --db {params.db} --out {output}"

rule hmmscan:
	input: config['fasta_file']
	output: "infiles/hmmscan.out"
	params: db = config['hmmscan_pfamdb']
	shell: "hmmscan --tblout {output} {params.db} {input}"
